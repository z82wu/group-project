#ifndef _ACBUILDING_H_
#define _ACBUILDING_H_
#include <string>
#include "property.h"
class AcBuilding: public Property{
	int MaxBlockNum;
 public:
  AcBuilding();
  ~AcBuilding();
  void improve();
  int charges() override;
  int GetMaxBlockNum();
};

#endif
