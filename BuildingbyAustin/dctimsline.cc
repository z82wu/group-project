#include "dctimsline.h"
#include <string>

dcTimsLine::dcTimsLine(){
  name = "DC TIMS LINE";
}

void arrive(Player *arriver){
	if (arriver->GetTimLine()){
		int time = arriver->GetTimDice();
		cout << "This is your " << time << "turn in TimsLine." << endl << "Please enter roll." << endl;
		std::string roll;
		while (cin >> roll){
			int dice1 = std::rand() % 6 + 1;
			int dice2 = std::rand() % 6 + 1;
			if (time < 3){
				if (dice1 == dice2){
					arriver->move(dice1+dice2);
					arriver->SetTimLine(false);
					arriver->SetTimDine(0);
				}
				else{
					arriver->SetTimDine(time + 1);
				}
			}
			else{
				if (dice1 == dice2){
					arriver->move(dice1+dice2);
					arriver->SetTimLine(false);
					arriver->SetTimDine(0);
				}
				else{
					cout << "You fail to roll double in three turns, Please enter pay for pay or card for using a card." << endl;
					std::string input;
					if (input == pay){
						arriver->LoseMoney(50);
						arriver->SetTimLine(false);
						arriver->SetTimDine(0);
					}
					else{
						arriver->SetNumOfCup(arriver->GetNumOfCup()-1);
						arriver->SetTimLine(false);
						arriver->SetTimDine(0);
					}
				}
			}
		}
	}
}
