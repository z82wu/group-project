#include "acbuilding.h"
#include <string>
#include <iostream>

AcBuilding::AcBuilding(){
  type = "acbuilding";
  tuitionFee = new int [6];
  improvable = true;
}

AcBuilding::~AcBuilding(){
  delete tuitionFee;
}

AcBuilding::improve(){
	improved += 1;
	owner->LoseMoney(improvCost);
}

int AcBuilding::GetMaxBlockNum(){
	return MaxBlockNum;
}

int AcBuilding::charges(){
	int NormalCharge = tuitionFee[improved];
	if (owner->Monopoly(this)){
		NormalCharge *=2;
	}
	return NormalCharge;
}
